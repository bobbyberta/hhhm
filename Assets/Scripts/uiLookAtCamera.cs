﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class uiLookAtCamera : MonoBehaviour {

	public Camera mainCamera;

	// Use this for initialization
	void Start () {

//		mainCamera = GameObject.Find ("Main Camera");
		
	}
	
	// Update is called once per frame
	void Update () {

		//Vector3 rotated = new Vector3 (90, 180, 0);
		transform.LookAt (transform.position + mainCamera.transform.rotation * Vector3.back, mainCamera.transform.rotation * Vector3.up);


//		GameObject.Find ("Main Camera").transform, Vector3.up);
//		transform.eulerAngles += rotated;
		
	}
}
