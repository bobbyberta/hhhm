﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Audio;
using UnityEngine;

public class musicControlls : MonoBehaviour {

	public AudioMixerSnapshot background;
	public AudioMixerSnapshot alarm;

	//Need to change this bpm to the actual bpm of the background track
	public float bpm = 128;

	private float m_TransitionIn;
	private float m_TranstionOut;
	private float m_QuarterNote;

	// Use this for initialization
	void Start () {

		m_QuarterNote = 60 / bpm;
		m_TransitionIn = m_QuarterNote;
		m_TranstionOut = m_QuarterNote * 4;
		
	}

	void OnEnable ()
	{
		playerManager.dieing += PlayAlarm;
	}


	void OnDisable ()
	{
		playerManager.dieing -= PlayAlarm;
	}

	void PlayAlarm (bool dieing)
	{
		if (dieing == true) {
//			Debug.Log ("I should hear beeping");
			alarm.TransitionTo (m_TransitionIn);

		} else if (dieing == false) {
//			Debug.Log ("Beeping should stop");
			background.TransitionTo (m_TranstionOut);
		}else {
			//Debug.Log ("Something has gone wrong");
		}
	}

}
