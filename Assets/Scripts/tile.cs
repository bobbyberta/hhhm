﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class tile : MonoBehaviour {

	public string tileColour;


	public void SetColour(string colour) {
		tileColour = colour;
	}

	public string GetColor() {
		return tileColour;
	}

	void Awake() {
		tileColour = "NON";
	}

	void UpdateColour() {
		
		GetComponent<Renderer> ().material.color = GameObject.Find ("Level").GetComponent<colour> ().GetColours () [tileColour];

	}

	void Update(){
		UpdateColour ();
	
	}
}
