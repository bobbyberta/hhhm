﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadSceneOnClick : MonoBehaviour {

	private int currentScene;
	private int nextScene;

	public void LoadByIndex(int sceneIndex)
	{
		SceneManager.LoadScene (sceneIndex);
	}

	public void LoadByCurrentIndex(){

		currentScene = gameStats.control.currentLevel;
	
		SceneManager.LoadScene (currentScene);
	}

	public void LoadByNextIndex(){

		nextScene = gameStats.control.currentLevel + 1;

		if (nextScene >= 9) {

			SceneManager.LoadScene (1);
		
		} else {
			SceneManager.LoadScene (nextScene);
		}
	}
}
