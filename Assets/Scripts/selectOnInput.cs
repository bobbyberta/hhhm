﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class selectOnInput : MonoBehaviour {

	public EventSystem eventSystem;
	public GameObject selectedObject;

	private bool buttonSelected;


	// Update is called once per frame
	void Update () 
	{
		if (Input.GetAxisRaw ("1y") != 0 && buttonSelected == false) 
		{
			eventSystem.SetSelectedGameObject(selectedObject);
			buttonSelected = true;
		}
	}

	private void OnDisable()
	{
		buttonSelected = false;
	}
}