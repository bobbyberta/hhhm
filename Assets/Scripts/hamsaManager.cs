﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class hamsaManager : MonoBehaviour
{
	public float maxHamsaHealth;
	public int healingPower;

	//delegates are used to send information
	public delegate void HamsaHealth (bool broken);
	public static event HamsaHealth broken;

	private GameObject[] hamsasInLevel;
	public int hamsasToColour;

	void OnEnable ()
	{
		hamsa.hamsaColourNeeded += ReassignColour;
	}

	void OnDisable ()
	{
		hamsa.hamsaColourNeeded -= ReassignColour;
	}

	void Start ()
	{

		hamsasInLevel = GameObject.Find ("Level").GetComponent<levelManager> ().GetHamsasInLevel ();

		AssignInitialColours ();
	}

	void AssignInitialColours ()
	{

		List<string> chosenColours = new List<string> ();
		List<int> chosenHamsas = new List<int> ();

		string[] colourNames = GameObject.Find ("Level").GetComponent<colour> ().GetColourNames ();

		for (int i = 0; i < hamsasInLevel.Length; i++) {

			int index = (int)Random.Range (0f, hamsasInLevel.Length);

			if (chosenHamsas.Contains (index)) {
				i -= 1;
			} else {
				string pickedColor = colourNames [(int)Random.Range (0f, colourNames.Length)];

				if (pickedColor == "NON") {
					i -= 1;
				} else if (chosenColours.Contains (pickedColor)) {
					i -= 1;
				} else {
					chosenColours.Add (pickedColor);
					chosenHamsas.Add (index);
				}
			}
		}

		for (int j = 0; j < hamsasToColour; j++) {

			int index = (int)Random.Range (0f, hamsasInLevel.Length);

			if (chosenHamsas.Contains (index)) {
				hamsasInLevel [index].GetComponent<hamsa> ().SetColour (chosenColours [j]);
				chosenHamsas.Remove (index);
			} else {
				j -= 1;
			}

		}
	}

	void ReassignColour ()
	{
		List<string> currentColours = new List<string> ();
		List<string> newColours = new List<string> ();
		List<int> currentHamsa = new List<int> ();

		string[] colourNames = GameObject.Find ("Level").GetComponent<colour> ().GetColourNames ();

		//Get colours that exist in the world - lanterns and floor tiles
		//Add them to the list currentColours
		for (int i = 0; i < hamsasInLevel.Length; i++) {
			if (hamsasInLevel [i].GetComponent<hamsa> ().GetColor () != "NON") {
				currentColours.Add (hamsasInLevel [i].GetComponent<hamsa> ().GetColor ());
			}
		}


		//Find a new ranom Colour
		for (int j = 0; j < colourNames.Length; j++) {
			if (!currentColours.Contains (colourNames [j]) && colourNames [j] != "NON") {
				newColours.Add (colourNames [j]);
			} 
		}

		//Make a list of floor tiles that can be colour
		for (int p = 0; p < hamsasInLevel.Length; p++) {
			if (hamsasInLevel [p].GetComponent<hamsa> ().GetColor () == "NON") {
				currentHamsa.Add (p);
			} 
		}

		//Choose a random floor colour that doesn't have a colour and assign a new one
		for (int r = 0; r < 1; r++) {

			int indexTile = (int)Random.Range (0f, hamsasInLevel.Length);
			int indexColour = (int)Random.Range (0f, newColours.Count);

			if (hamsasInLevel [indexTile].GetComponent<hamsa> ().GetColor () != "NON") {
				r -= 1;
			} else {
				hamsasInLevel [indexTile].GetComponent<hamsa> ().SetColour (newColours [indexColour]);
			}
		}

	}

	void Update ()
	{
		CheckForBrokenHamsas ();
	}


	void CheckForBrokenHamsas ()
	{

		List<float> deadHamsas = new List<float> ();

		//Add any Hamsas's with 0 health into a list called deadHansas
		for (int i = 0; i < hamsasInLevel.Length; i++) {

			if (hamsasInLevel [i].GetComponent<hamsa> ().hamsaHealth == 0) {

				deadHamsas.Add (hamsasInLevel [i].GetComponent<hamsa> ().hamsaHealth);

				//broken (true);

			} else if (hamsasInLevel [i].GetComponent<hamsa> ().hamsaHealth > 0) {

				deadHamsas.Remove (hamsasInLevel [i].GetComponent<hamsa> ().hamsaHealth);

			}
		}
		if (deadHamsas.Count == 0) {
			broken (false);
			//Debug.Log ("false");
		} else {
			broken (true);
			//Debug.Log ("true");
		}
	}
}
