﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class displayStats : MonoBehaviour {

	public Text playerDead;

	void Start(){

		SetPlayerDeadText ();
	
	}

	void SetPlayerDeadText ()
	{
		playerDead.text = "You let " + gameStats.control.playerWhoDied + " die!";

	}
}
