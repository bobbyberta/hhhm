﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class player : MonoBehaviour
{

	public string playerColour;
	public string playerName;
	public string lanternColour;
	public float playerHealth;
	public Image playerHealthBar;
	public Image playerHealthBarBase;

	private float maxPlayerHealth;
	private int healingPower;

	private GameObject colourThisBody;
	private GameObject colourThisHat;
	private GameObject colourThisSkirt;
	private GameObject colourThisBase;

	private int scalingFramesLeft;

	private GameObject lantern;
	private GameObject healZone;

	//delegates are used to send information
	public delegate void PlayerLiving (bool dead);
	public static event PlayerLiving died;

	//delegates are used to send information
	public delegate void NewColorNeeded ();
	public static event NewColorNeeded colourNeeded;


	void Awake ()
	{
		playerColour = "NON";
		playerName = this.name;
		lanternColour = "NON";

	}

	void Start ()
	{

		maxPlayerHealth = GameObject.Find ("Players").GetComponent<playerManager> ().maxPlayerHealth;
		healingPower = GameObject.Find ("Players").GetComponent<playerManager> ().healingPower;

		playerHealth = maxPlayerHealth;
	
		UpdateColour ();
		UpdateHealthBar ();

	}

	public void SetColour (string colour)
	{
		playerColour = colour;
	}

	public string GetColor ()
	{
		return playerColour;
	}

	public string GetLantern ()
	{
		return lanternColour;
	}

	public void SetLantern (string colour)
	{
		lanternColour = colour;
	}

	void OnEnable ()
	{
		pulse.timeUp += TakeDamage;
		collect.collected += UpdateLantern;
		heal.removeColour += HealingAnotherPlayer;

	}

	void OnDisable ()
	{
		pulse.timeUp -= TakeDamage;
		collect.collected -= UpdateLantern;
		heal.removeColour -= HealingAnotherPlayer;
	}



	void TakeDamage (int damage)
	{

		if (playerHealth <= 0) {
			playerHealth = 0;

			died (true);


		} else {
			playerHealth -= damage;
		}
	}

	void TakeHealth (int healingPower)
	{

		playerHealth += healingPower;

		if (playerHealth >= maxPlayerHealth) {
			playerHealth = maxPlayerHealth;

		}
	}

	void UpdateColour ()
	{

		switch (playerName) {
		case "player1":
			colourThisBody = GameObject.Find ("Body1");
			colourThisHat = GameObject.Find ("Hat1");
			colourThisSkirt = GameObject.Find ("Skirt1");
			colourThisBase = GameObject.Find ("Base1");
			break;
		case "player2":
			colourThisBody = GameObject.Find ("Body2");
			colourThisHat = GameObject.Find ("Hat2");
			colourThisSkirt = GameObject.Find ("Skirt2");
			colourThisBase = GameObject.Find ("Base2");
			break;
		case "player3":
			colourThisBody = GameObject.Find ("Body3");
			colourThisHat = GameObject.Find ("Hat3");
			colourThisSkirt = GameObject.Find ("Skirt3");
			colourThisBase = GameObject.Find ("Base3");
			break;
		case "player4":
			colourThisBody = GameObject.Find ("Body4");
			colourThisHat = GameObject.Find ("Hat4");
			colourThisSkirt = GameObject.Find ("Skirt4");
			colourThisBase = GameObject.Find ("Base4");
			break;
		}


		colourThisBody.GetComponent<SkinnedMeshRenderer> ().material.color = GameObject.Find ("Level").GetComponent<colour> ().GetColours () [playerColour];
		colourThisHat.GetComponent<SkinnedMeshRenderer> ().material.color = GameObject.Find ("Level").GetComponent<colour> ().GetColours () [playerColour];
		colourThisSkirt.GetComponent<SkinnedMeshRenderer> ().material.color = GameObject.Find ("Level").GetComponent<colour> ().GetColours () [playerColour];
		colourThisBase.GetComponent<Image> ().color = GameObject.Find ("Level").GetComponent<colour> ().GetColours () [playerColour];

		//Test if it looks better with colour or without!

		//playerHealthBar.GetComponent<Image> ().color = GameObject.Find ("Level").GetComponent<colour> ().GetColours () [playerColour];

	}

	public void UpdateLantern ()
	{


		switch (playerName) {
		case "player1":
			lantern = GameObject.Find ("Lantern1");
			break;
		case "player2":
			lantern = GameObject.Find ("Lantern2");
			break;
		case "player3":
			lantern = GameObject.Find ("Lantern3");
			break;
		case "player4":
			lantern = GameObject.Find ("Lantern4");
			break;
		}

		//lantern.GetComponent<Renderer> ().material.color = GameObject.Find ("Level").GetComponent<colour> ().GetColours () [lanternColour];
		//lantern.GetComponent<Renderer> ().material.color = GameObject.Find ("Level").GetComponent<colour> ().GetColours () [lanternColour];
		lantern.GetComponent<Image> ().color = GameObject.Find ("Level").GetComponent<colour> ().GetColours () [lanternColour];

	}

	void HealingAnotherPlayer ()
	{
		//add counter here for how many times you healed other players
		UpdateLantern ();
	
	}

	public void Healing ()
	{
		TakeHealth (healingPower);

		playerColour = "NON";
		colourNeeded ();
		UpdateColour ();

		//Debug.Log (playerHealth);
	}

	void Update ()
	{
		UpdateHealthBar ();
	}

	void UpdateHealthBar ()
	{

		float ratio = playerHealth / maxPlayerHealth;

		playerHealthBar.fillAmount = ratio;

	}


}
		
	