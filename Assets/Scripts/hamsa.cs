﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class hamsa : MonoBehaviour
{

	public string hamsaColour;
	public float hamsaHealth;
	public Image hamsaHealthBar;

	private float maxHamsaHealth;
	private int healingPower;

	private Color currentColour;
	private Color fadedColour;
	private Color fade;

	//delegates are used to send information
	public delegate void NewHamsaColorNeeded ();
	public static event NewHamsaColorNeeded hamsaColourNeeded;


	void Awake ()
	{
		hamsaColour = "NON";
	}

	public void SetColour (string colour)
	{
		hamsaColour = colour;
	}

	public string GetColor ()
	{
		return hamsaColour;
	}

	void OnEnable ()
	{
		pulse.timeUp += TakeDamage;
	}

	void OnDisable ()
	{
		pulse.timeUp -= TakeDamage;
	}


	void Start ()
	{
		maxHamsaHealth = GameObject.Find ("Hamsas").GetComponent<hamsaManager> ().maxHamsaHealth;
		healingPower = GameObject.Find ("Hamsas").GetComponent<hamsaManager> ().healingPower;

		hamsaHealth = maxHamsaHealth;

		UpdateHealthBar ();
	}

	void TakeDamage (int damage)
	{

		if (hamsaHealth <= 0) {
			hamsaHealth = 0;
		} else {
			hamsaHealth -= damage;
		}
	}

	void TakeHealth (int healingPower) {

		hamsaHealth += healingPower;

		if (hamsaHealth >= maxHamsaHealth) {
			hamsaHealth = maxHamsaHealth;

		}
	}

	public void Healing ()
	{
		TakeHealth (healingPower);

		hamsaColour = "NON";
		hamsaColourNeeded ();
		UpdateColour ();

		//Debug.Log (playerHealth);
	}

	void UpdateColour ()
	{
		Color fade = new Vector4 (0f, 0f, 0f, 0.25f);

		GetComponent<Renderer> ().material.color = GameObject.Find ("Level").GetComponent<colour> ().GetColours () [hamsaColour];

		currentColour = GetComponent<Renderer> ().material.color;

		fadedColour = currentColour - fade;

		GetComponent<Renderer> ().material.color = fadedColour;
	}

	void Update ()
	{
		UpdateColour ();
		UpdateHealthBar ();

	}

	void UpdateHealthBar(){

		float ratio = hamsaHealth / maxHamsaHealth;

		hamsaHealthBar.fillAmount = ratio;

		//hamsaHealthBar.rectTransform.localScale = new Vector3 (ratio, 1, 1);


	}
}
