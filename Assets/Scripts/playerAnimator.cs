﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerAnimator : MonoBehaviour {

	public string verticalInstruction;
	public string horizontalInstruction;

	void Update(){

		float xAxis = Mathf.Abs(Input.GetAxis(verticalInstruction));
		float yAxis = Mathf.Abs(Input.GetAxis(horizontalInstruction));

		float move = xAxis + yAxis;
			

		GetComponent<Animator>().SetFloat("moving", move);

	}
}
