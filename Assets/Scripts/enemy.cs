﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class enemy : MonoBehaviour {

	public float enemyHealth;
	public float maxEnemyHealth;

	public Image enemyHealthBar;


	//delegates are used to send information
	public delegate void EnemyNearlDead();
	public static event EnemyNearlDead enemyLowHealth;

	void OnEnable()
	{
		attack.timeUp += TakeDamage;
	}


	void OnDisable()
	{
		attack.timeUp -= TakeDamage;
	}

	void Start(){
		
		enemyHealth = maxEnemyHealth;

		UpdateHealthBar ();
	}

	void Update(){

		UpdateHealthBar ();
	
		if (enemyHealth <= 0) {
			SceneManager.LoadScene (3);
			//Debug.Log ("You have won!");
		} else if((enemyHealth / maxEnemyHealth) < 0.1){

			//enemyLowHealth ();
		}
	}

	void UpdateHealthBar(){

		float ratio = enemyHealth / maxEnemyHealth;
		enemyHealthBar.rectTransform.localScale = new Vector3 (1, ratio, 1);

	}
		
	void TakeDamage (int damage) {

		if (enemyHealth <= 0) {
			enemyHealth = 0;
		} else {
			enemyHealth -= damage;
		}
	}
}
