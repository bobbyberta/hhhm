﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class tileManager : MonoBehaviour {

	private GameObject[] tilesInLevel;
	private GameObject[] playersInLevel;
	public int tilesToColour;

	void OnEnable()
	{
		collect.collected += ReassignColour;
		heal.colourReleased += ReassignColour;
	}

	void OnDisable()
	{
		collect.collected -= ReassignColour;
		heal.colourReleased -= ReassignColour;
	}

	void Start(){
		tilesInLevel = GameObject.Find ("Level").GetComponent<levelManager> ().GetTilesInLevel ();
		playersInLevel = GameObject.Find ("Level").GetComponent<levelManager> ().GetPlayersInLevel ();
		AssignInitialColours ();
	}

	void AssignInitialColours(){

		List<string> chosenColours = new List<string>();
		List<int> chosenTiles = new List<int> ();

		string[] colourNames = GameObject.Find ("Level").GetComponent<colour> ().GetColourNames ();

		for (int i = 0; i < tilesToColour; i++) {

			int index = (int)Random.Range (0f, tilesInLevel.Length);

			if (chosenTiles.Contains (index)) {
				i -= 1;
			} 
			else {
				string pickedColor = colourNames [(int)Random.Range (0f, colourNames.Length)];

				if (pickedColor == "NON") {
					i -= 1;
				}
				else if (chosenColours.Contains (pickedColor)) {
					i -= 1;
				} 
				else {
					chosenColours.Add (pickedColor);
					chosenTiles.Add (index);
				}
			}
		}

		//This colours the tiles - based on how many colours there are
		for (int j = 0; j < tilesToColour; j++) {

			int index = (int)Random.Range (0f, tilesInLevel.Length);

			if (chosenTiles.Contains (index)) {
				tilesInLevel [index].GetComponent<tile> ().SetColour (chosenColours [j]);
				chosenTiles.Remove (index);
			} else {
				j -= 1;
			}

		}
	}

	void ReassignColour() {
		List<string> currentColours = new List<string> ();
		List<string> newColours = new List<string> ();
		List<int> currentTiles = new List<int> ();

		string[] colourNames = GameObject.Find ("Level").GetComponent<colour> ().GetColourNames ();

		//Get colours that exist in the world - lanterns and floor tiles
		//Add them to the list currentColours
		for (int i = 0; i < tilesInLevel.Length; i++) {
			if (tilesInLevel [i].GetComponent<tile> ().GetColor () != "NON") {
				currentColours.Add (tilesInLevel [i].GetComponent<tile> ().GetColor ());
			}
		}
		//Add player lantern colours to the current colour list	
		for (int l = 0; l < playersInLevel.Length; l++) {
			if (playersInLevel [l].GetComponent<player> ().GetLantern () != "NON") {
				currentColours.Add (playersInLevel [l].GetComponent<player> ().GetLantern ());
			}
		}


		//Find a new ranom Colour
		for (int j = 0; j < colourNames.Length; j++) {
			if(!currentColours.Contains(colourNames[j]) && colourNames[j] != "NON") {
				newColours.Add (colourNames[j]);
			} 
		}

		//only find a new colour if there is one avalible (the colour list has something in it)
		if(newColours.Count > 0){

			//need to run this function when a lantern has been used to heal something as well...
			
			//Make a list of floor tiles that can be colour
			for (int p = 0; p < tilesInLevel.Length; p++) {
				if (tilesInLevel [p].GetComponent<tile> ().GetColor () == "NON") {
					currentTiles.Add (p);
				} 
			}

			//calculate the number of coloured tiles in order to check that there is never more than max tilesToColour number
			int numberOfColouredTiles = tilesInLevel.Length - currentTiles.Count;

			//If there is a tile to colour
			if(numberOfColouredTiles < tilesToColour){

				//Choose a random floor colour that doesn't have a colour and assign a new one
				for(int r = 0; r < 1; r++){

					int indexTile = (int)Random.Range (0f, tilesInLevel.Length);
					int indexColour = (int)Random.Range (0f, newColours.Count);

					if (tilesInLevel [indexTile].GetComponent<tile> ().GetColor() != "NON") {
						r -= 1;
					} else {
						tilesInLevel [indexTile].GetComponent<tile> ().SetColour (newColours[indexColour]);
					}
				}
			}
		}

	}
}