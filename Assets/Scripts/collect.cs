﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class collect : MonoBehaviour {

	public string collectInstruction;

	//delegates are used to send information
	public delegate void ColourHasBeenCollected ();
	public static event ColourHasBeenCollected collected;

//	void Update(){
//		if (Input.GetButtonDown (collectInstruction)) {
//			Debug.Log ("Button has been pressed");
//		}
//	
//	}


	void OnTriggerStay(Collider coll) {
		switch (coll.gameObject.tag) {
		case "Tile":
			if (Input.GetButtonDown (collectInstruction)) {
				CollectColour (coll.gameObject);
			}
			break;
		}
	}

	void CollectColour(GameObject tile) {
		if (tile.GetComponent<tile> ().GetColor () != "NON" && GetComponent<player>().GetLantern() == "NON") {

			if (tile.GetComponent<tile> ().GetColor () != GetComponent<player> ().GetColor ()) {
			
				GetComponent<player>().SetLantern(tile.GetComponent<tile>().GetColor());
				tile.GetComponent<tile> ().SetColour ("NON");

				//Send a message to the tile Mangaer that this colour is being held by a lantern?
				//

				collected ();
			
			}
		}
	}

}
