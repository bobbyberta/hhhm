﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pulse : MonoBehaviour {

	public int pulseDelay;
	public int pulseAttackValue;

	private Light lt;

	//Do I need pulseAwake?
	private bool pulseAwake;
	private float countDown;

	//delegates are used to send information
	public delegate void PulseDamage(int pulseAttackValue);
	public static event PulseDamage timeUp;


	void Start(){

		lt = GameObject.Find ("Directional Light").GetComponent<Light> ();
		lt.intensity = 1;

		countDown = pulseDelay;
	}


	void Update(){

		countDown -= Time.deltaTime;

		//lt.intensity = 1;

		if (Mathf.Floor (countDown) <= 0) {

			if(timeUp != null)
				timeUp(pulseAttackValue);
				StartCoroutine("AnimatePulseAttack");

			//Reset the countDown timer
			countDown = pulseDelay;

		}
	}

	IEnumerator AnimatePulseAttack(){

		if (lt.intensity == 1f) {
			lt.intensity = 1.2f;
			yield return new WaitForSeconds(1.0f);
			lt.intensity = 1f;
		}
	}
}

