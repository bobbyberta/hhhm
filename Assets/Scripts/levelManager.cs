﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class levelManager : MonoBehaviour {

	public int currentLevel;

	public GameObject[] players;
	private GameObject[] tiles;
	private GameObject[] hamsas;
	public GameObject[] tutorialUI;
	public GameObject[] sounds;

	public GameObject step0;
	public GameObject step1;
	public GameObject step2;
	public GameObject step3;
	public GameObject step4;
	public GameObject step5;
	public GameObject step6;
	public GameObject step7;
	public GameObject tutorialBase;


	void Awake () {
		players = GameObject.FindGameObjectsWithTag ("Player");
		tiles = GameObject.FindGameObjectsWithTag ("Tile");
		hamsas = GameObject.FindGameObjectsWithTag ("Hamsa");
		tutorialUI = GameObject.FindGameObjectsWithTag ("TutorialUI");
		sounds = GameObject.FindGameObjectsWithTag ("Sound");



	}

	public GameObject[] GetPlayersInLevel() {
		return players;
	}

	public GameObject[] GetHamsasInLevel() {
		return hamsas;
	}

	public GameObject[] GetTilesInLevel() {
		return tiles;
	}

	void Start(){
		
		gameStats.control.currentLevel = currentLevel;

		//UnPauseGame ();

		StartTutorial ();


	}

	public void StartTutorial(){

		gameStats.control.tutorialMode = true;

		Time.timeScale = 0;

		foreach (GameObject player in players)
		{
			player.SetActive (false);
		}

		foreach (GameObject sound in sounds)
		{
			sound.SetActive (false);
		}

		foreach (GameObject show in tutorialUI)
		{
			show.SetActive (true);
		}

		step1.SetActive (false);
		step2.SetActive (false);
		step3.SetActive (false);
		step4.SetActive (false);
		step5.SetActive (false);
		step6.SetActive (false);
		step7.SetActive (false);


		tutorialBase.SetActive (true);
		step0.SetActive (true);
//
//		Debug.Log ("end of this");
			
	}

	public void UnPauseGame(){

		Debug.Log ("I have been pressed!!");

		gameStats.control.paused = false;
		gameStats.control.tutorialMode = false;

		foreach (GameObject player in players)
		{
			player.SetActive (true);
		}

		foreach (GameObject sound in sounds)
		{
			sound.SetActive (true);
		}

		foreach (GameObject show in tutorialUI)
		{
			show.SetActive (false);
		}

		Time.timeScale = 1;

	}
}
