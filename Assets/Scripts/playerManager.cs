﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class playerManager : MonoBehaviour {

	private GameObject[] playersInLevel;

	public float maxPlayerHealth;
	public int healingPower;

	public float warningAtHealthPercentage;

	public string deadPlayer;

	public Transform canvas;

	//delegates are used to send information
	public delegate void PlayerHealth (bool dieing);
	public static event PlayerHealth dieing;

	void Start(){
		playersInLevel = GameObject.Find ("Level").GetComponent<levelManager> ().GetPlayersInLevel ();

		AssignInitialColours ();

		gameStats.control.playerWhoDied = "";

	}

	void Update(){

		if(gameStats.control.tutorialMode != true){
			CheckForDieingPlayers ();	
		}
	}
	
	void OnEnable()
	{
		player.died += EndOfGame;
		player.colourNeeded += ReassignPlayerColour;
	}
		
	void OnDisable()
	{
		player.died -= EndOfGame;
		player.colourNeeded -= ReassignPlayerColour;
	}

	void EndOfGame(bool dead){
		if (dead == true) {

			CheckWhoDied ();

			//Debug.Log ("The game has ended");
			SceneManager.LoadScene (2);
		}else{
			//Continue as normal
		}
	}

	void AssignInitialColours(){

		List<string> chosenColours = new List<string>();
		List<int> chosenPlayers = new List<int> ();

		string[] colourNames = GameObject.Find ("Level").GetComponent<colour> ().GetColourNames ();

		for (int i = 0; i < playersInLevel.Length; i++) {

			int index = (int)Random.Range (0f, playersInLevel.Length);

			if (chosenPlayers.Contains (index)) {
				i -= 1;
			} 
			else {
				string pickedColor = colourNames [(int)Random.Range (0f, colourNames.Length)];

				if (pickedColor == "NON") {
					i -= 1;
				}
				else if (chosenColours.Contains (pickedColor)) {
					i -= 1;
				} 
				else {
					chosenColours.Add (pickedColor);
					chosenPlayers.Add (index);
				}
			}
				
		}

		//This colours the tiles - based on how many colours there are
		for (int j = 0; j < chosenPlayers.Count; j++) {
			
			playersInLevel [j].GetComponent<player> ().SetColour (chosenColours [j]);

		}
}
	void ReassignPlayerColour(){

		List<string> currentColours = new List<string> ();
		List<string> newColourList = new List<string> ();

		string[] colourNames = GameObject.Find ("Level").GetComponent<colour> ().GetColourNames ();

		//Get colour of players in the level
		for (int l = 0; l < playersInLevel.Length; l++) {
			if (playersInLevel [l].GetComponent<player> ().GetColor () != "NON") {
				currentColours.Add (playersInLevel [l].GetComponent<player> ().GetColor ());
			}
		}

		//Find a new random Colour
		for (int j = 0; j < colourNames.Length; j++) {
			if (!currentColours.Contains (colourNames [j]) && colourNames [j] != "NON") {
				newColourList.Add (colourNames [j]);
			}
		}
			
		//For a player who does not have a colour, assign a new colour
		for (int p = 0; p < playersInLevel.Length; p++) {

			int indexColour = (int)Random.Range (0f, newColourList.Count);

			if (playersInLevel [p].GetComponent<player> ().GetColor () == "NON") {
				playersInLevel [p].GetComponent<player> ().SetColour (newColourList[indexColour]);
			} 
		}
	}

	//This function looks for players who are running low on health and then broadcasts messages acordingly
	void CheckForDieingPlayers ()
	{

		List<float> dieingPlayers = new List<float> ();

		//Add any Hamsas's with 0 health into a list called deadHansas
		for (int i = 0; i < playersInLevel.Length; i++) {

			if ((playersInLevel [i].GetComponent<player> ().playerHealth / maxPlayerHealth) < warningAtHealthPercentage) {

				dieingPlayers.Add (playersInLevel [i].GetComponent<player> ().playerHealth);

				//dieing (true);

			} else if ((playersInLevel [i].GetComponent<player> ().playerHealth / maxPlayerHealth) <= warningAtHealthPercentage) {

				dieingPlayers.Remove (playersInLevel [i].GetComponent<player> ().playerHealth);

			}
		}

		if (dieingPlayers.Count == 0) {
			dieing (false);
//			Debug.Log ("false");
		} else {
			dieing (true);
//			Debug.Log ("true");
		}
	}

	void CheckWhoDied (){

		//Add any Players's with 0 health into a list called deadPlayers
		for (int i = 0; i < playersInLevel.Length; i++) {

			if (playersInLevel [i].GetComponent<player> ().playerHealth <= 0) {

				gameStats.control.playerWhoDied += (playersInLevel [i].GetComponent<player> ().playerName);

			}
		}
			
	}
}
