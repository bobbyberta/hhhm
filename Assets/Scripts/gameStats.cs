﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class gameStats : MonoBehaviour{

	public static gameStats control;

	public string playerWhoDied;
	public bool paused;
	public bool tutorialMode;

	public int currentLevel;

	void Awake(){

		playerWhoDied = "";

		if (control == null) {
			DontDestroyOnLoad (gameObject);
			control = this;
		} else if (control != this) {
			Destroy (gameObject);
		}
	}
}
