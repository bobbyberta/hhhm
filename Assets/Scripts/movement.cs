﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class movement : MonoBehaviour {

	public string verticalInstruction;
	public string horizontalInstruction;


	void Move() {

		float moveVertical = Input.GetAxis (verticalInstruction);
		float moveHorizontal = Input.GetAxis (horizontalInstruction);

		Vector3 movement = new Vector3 (moveHorizontal, 0.0f, moveVertical);


		//By putting it in a if statement the player continues facing where they are when they stop
		if (movement != Vector3.zero) {

			//Had to reverse the movement
			transform.rotation = Quaternion.LookRotation(-movement);

			//direction of movememt, speed of character, time, space.world keeps in rotating in the right directions.
			transform.Translate (movement * 40.0f * Time.deltaTime, Space.World);
		}

	}


	void Update(){

		Move ();
	
	}
}
