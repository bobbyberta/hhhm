﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class camera : MonoBehaviour {

	public Rigidbody player1;
	public Rigidbody player2;
	public Rigidbody player3;
	public Rigidbody player4;

	private Vector3 player1Position;
	private Vector3 player2Position;
	private Vector3 player3Position;
	private Vector3 player4Position;

	private Vector3 totalPosition;
	private Vector3 averagePositon;

	public Component thisCamera;
	private Vector3 cameraPosition;

	private Vector3 newCameraPosition;

	public float speed;

	public float distance1;
	public Vector3 offset1;

	public float distance2;
	public Vector3 offset2;

	public float distance3;
	public Vector3 offset3;

	public Quaternion cameraRotation;


	// Update is called once per frame
	void Update () {

		GetPlayerPositions ();
		GetAveragePosition ();
		GetCameraPosition ();
		GetNewCameraPosition ();
		MoveCamera ();

	}

	void GetPlayerPositions () {

		player1Position = player1.transform.position;
		player2Position = player2.transform.position;
		player3Position = player3.transform.position;
		player4Position = player4.transform.position;

	}

	void GetAveragePosition(){

		totalPosition = (player1Position + player2Position + player3Position + player4Position);
		averagePositon = (totalPosition / 4);

	}

	void GetCameraPosition () {

		cameraPosition = GetComponent<camera>().transform.position;

	}

	void GetNewCameraPosition () {


		if (averagePositon.magnitude <= distance1) {

			newCameraPosition = (averagePositon + offset1);

		} else if (averagePositon.magnitude <= distance2) {

			newCameraPosition = (averagePositon + offset2);

		} else if (averagePositon.magnitude <= distance3) {

			newCameraPosition = (averagePositon + offset3);

		}


	}

	void MoveCamera () {
		GetComponent<camera>().transform.position = Vector3.Lerp(transform.position, newCameraPosition, speed * Time.deltaTime);
		//GetComponent<camera>().transform.rotation = Quaternion.Lerp(transform.rotation, cameraRotation, speed * Time.deltaTime);
	}

}
