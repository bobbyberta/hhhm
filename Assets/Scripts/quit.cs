﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class quit : MonoBehaviour {

	public string quitInstrutionLeft;
	public string quitInstrutionRight;

	public bool paused;

	public GameObject[] pauseUI;
	public GameObject[] players;


	void Start(){

		pauseUI = GameObject.FindGameObjectsWithTag("PauseUI");
		players = GameObject.FindGameObjectsWithTag("Player");

		foreach (GameObject show in pauseUI)
		{
			show.SetActive (false);
		}

	}


	void Update(){

		Quit ();
		PauseTheGame ();

	}


	void Quit() {

		bool quitButtonLeft = Input.GetButton (quitInstrutionLeft);
		bool quitButtonRight = Input.GetButton (quitInstrutionRight);
	

		//By putting it in a if statement the player continues facing where they are when they stop
		if (quitButtonLeft || quitButtonRight) {

			gameStats.control.paused = true;

		}

	}

	void PauseTheGame(){

		if (gameStats.control.paused == true) {

			Time.timeScale = 0;

			foreach (GameObject show in pauseUI)
			{
				show.SetActive (true);
			}

			foreach (GameObject player in players)
			{
				player.SetActive (false);
			}
			}
		}
}
	


