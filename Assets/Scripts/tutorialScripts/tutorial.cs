﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class tutorial : MonoBehaviour {

	public GameObject splashScreen;
	public GameObject controlls;
	public GameObject step1;
	public GameObject step2;
	public GameObject step3;
	public GameObject step4;
	public GameObject step5;
	public GameObject step6;
	public GameObject step7;
	public GameObject step8;
	public GameObject step9;

	private GameObject[] arrows;
	public float countDown;

	public bool firstColour;
	public int collectedColours;
	public int howManyHeals;
	public bool firstHeal;
	public bool firstLowHealth;
	public int waitForOneMoreHeal;
	private bool firstDeadHamsa;

	// Use this for initialization
	void Start () {

		controlls.SetActive (true);
		splashScreen.SetActive (false);

		step1.SetActive (true);
		step2.SetActive (false);
		step3.SetActive (false);
		step4.SetActive (false);
		step5.SetActive (false);
		step6.SetActive (false);
		step7.SetActive (false);
		step8.SetActive (false);
		step9.SetActive (false);

		firstColour = true;
		collectedColours = 0;
		firstHeal = true;
		howManyHeals = 0;
		firstLowHealth = true;
		waitForOneMoreHeal = 200;
		firstDeadHamsa = true;

		arrows = GameObject.FindGameObjectsWithTag ("arrow");
		
	}

	void OnEnable ()
	{
		collect.collected += ColourHasBeenCollected;
		heal.removeColour += HealedAnotherPlayer;
		playerManager.dieing += PlayerIsAlmostDead;
		hamsaManager.broken += DeadHamsa;
		enemy.enemyLowHealth += EnemyNearlyDead;

	}

	void OnDisable ()
	{
		collect.collected -= ColourHasBeenCollected;
		heal.removeColour -= HealedAnotherPlayer;
		playerManager.dieing -= PlayerIsAlmostDead;
		hamsaManager.broken -= DeadHamsa;
		enemy.enemyLowHealth -= EnemyNearlyDead;
	}

	void Update(){
	
		CheckAmountOfHealing ();

//		countDown -= Time.deltaTime;
//
//		if (Mathf.Floor (countDown) <= 0) {
//
//			splashScreen.SetActive (false);
//
//		}
	
	}

	void ColourHasBeenCollected(){

		collectedColours += 1;

		if (firstColour == true) {
			
			step1.SetActive (false);
			step2.SetActive (true);
			step3.SetActive (false);
			step4.SetActive (false);
			step5.SetActive (false);
			step6.SetActive (false);
			step7.SetActive (false);
			step8.SetActive (false);
			step9.SetActive (false);

			foreach (GameObject show in arrows) {
				show.SetActive (false);
			}

			firstColour = false;

		}

		if (collectedColours == 2) {
		
			step1.SetActive (false);
			step2.SetActive (false);
			step3.SetActive (true);
			step4.SetActive (false);
			step5.SetActive (false);
			step6.SetActive (false);
			step7.SetActive (false);
			step8.SetActive (false);
			step9.SetActive (false);

		}
	}

	void HealedAnotherPlayer(){

		howManyHeals += 1;

//		if (howManyHeals == 0) {
//
//			step1.SetActive (false);
//			step2.SetActive (false);
//			step3.SetActive (true);
//			step4.SetActive (false);
//			step5.SetActive (false);
//			step6.SetActive (false);
//			step7.SetActive (false);
//			step8.SetActive (false);
//			step9.SetActive (false);
//
//
//			howManyHeals = 1;
//
//		} else {
//			
//			howManyHeals += 1;
//		}
		
	}

	void CheckAmountOfHealing(){

		if (howManyHeals == 1) {

			step1.SetActive (false);
			step2.SetActive (false);
			step3.SetActive (false);
			step4.SetActive (true);
			step5.SetActive (false);
			step6.SetActive (false);
			step7.SetActive (false);
			step8.SetActive (false);
			step9.SetActive (false);
		}

		if (howManyHeals ==5){

			step1.SetActive (false);
			step2.SetActive (false);
			step3.SetActive (false);
			step4.SetActive (false);
			step5.SetActive (true);
			step6.SetActive (false);
			step7.SetActive (false);
			step8.SetActive (false);
			step9.SetActive (false);
			
		}

		if (howManyHeals == waitForOneMoreHeal) {

			step1.SetActive (false);
			step2.SetActive (false);
			step3.SetActive (false);
			step4.SetActive (false);
			step5.SetActive (false);
			step6.SetActive (true);
			step7.SetActive (false);
			step8.SetActive (false);
			step9.SetActive (false);
		}

		if (howManyHeals == 8){

			step1.SetActive (false);
			step2.SetActive (false);
			step3.SetActive (false);
			step4.SetActive (false);
			step5.SetActive (false);
			step6.SetActive (false);
			step7.SetActive (true);
			step8.SetActive (false);
			step9.SetActive (false);
		}

//		if (howManyHeals >= 12){
//
//			step1.SetActive (false);
//			step2.SetActive (false);
//			step3.SetActive (false);
//			step4.SetActive (false);
//			step5.SetActive (false);
//			step6.SetActive (false);
//			step7.SetActive (false);
//			step8.SetActive (true);
//		}

	}

	void PlayerIsAlmostDead(bool isDead){

		//Debug.Log (isDead);

		if(isDead == true){
			if (firstHeal == true) {
				
				step1.SetActive (false);
				step2.SetActive (false);
				step3.SetActive (false);
				step4.SetActive (false);
				step5.SetActive (false);
				step6.SetActive (true);
				step7.SetActive (false);
				step8.SetActive (false);
				step9.SetActive (false);

				firstHeal = false;

				waitForOneMoreHeal = howManyHeals + 1;

			}
		}
	}

	void DeadHamsa(bool isBroken){

		if (isBroken == true) {
			
			if (firstDeadHamsa == true) {
				
				step1.SetActive (false);
				step2.SetActive (false);
				step3.SetActive (false);
				step4.SetActive (false);
				step5.SetActive (false);
				step6.SetActive (false);
				step7.SetActive (true);
				step8.SetActive (false);
				step9.SetActive (false);

				firstDeadHamsa = false;

			}
		}
	}

	void EnemyNearlyDead(){

		step1.SetActive (false);
		step2.SetActive (false);
		step3.SetActive (false);
		step4.SetActive (false);
		step5.SetActive (false);
		step6.SetActive (false);
		step7.SetActive (false);
		step8.SetActive (false);
		step9.SetActive (true);
	
	}
}
