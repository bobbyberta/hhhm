﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class tutorialCollect : MonoBehaviour {

	public string collectInstruction;

	//delegates are used to send information
	public delegate void ColourHasBeenCollected ();
	public static event ColourHasBeenCollected collected;

	//	void Update(){
	//		if (Input.GetButtonDown (collectInstruction)) {
	//			Debug.Log ("Button has been pressed");
	//		}
	//	
	//	}


	void OnTriggerStay(Collider coll) {
		switch (coll.gameObject.tag) {
		case "Tile":
			if (Input.GetButtonDown (collectInstruction)) {
				CollectColour (coll.gameObject);
			}
			break;
		}
	}

	void CollectColour(GameObject tile) {
		if (tile.GetComponent<tutorialTileColour> ().GetColor () != "NON" && GetComponent<tutorialPlayerColour>().GetLantern() == "NON") {

			if (tile.GetComponent<tutorialTileColour> ().GetColor () != GetComponent<tutorialPlayerColour> ().GetColor ()) {

				GetComponent<tutorialPlayerColour>().SetLantern(tile.GetComponent<tutorialTileColour>().GetColor());
				tile.GetComponent<tutorialTileColour> ().SetColour ("NON");

				//Send a message to the tile Mangaer that this colour is being held by a lantern?
				//

				collected ();

			}
		}
	}

}
