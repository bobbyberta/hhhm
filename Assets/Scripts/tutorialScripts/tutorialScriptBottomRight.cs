﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class tutorialScriptBottomRight : MonoBehaviour {

	public GameObject BottomRight;
	public GameObject ImageBackground;
	public GameObject TextStart;
	public GameObject ImageBg;
	public GameObject ImageController;
	public GameObject TextCollect;
	public GameObject TextHeal;
	public GameObject BottomBarrier;

	private GameObject player3;
	private GameObject player4;



	// Use this for initialization
	void Start () {

		player3 = GameObject.Find ("player3");
		player4 = GameObject.Find ("player4");

		tutorialStep1 ();


	}


	// Update is called once per frame
	void Update () {

		if (player3.GetComponent<tutorialPlayerColour> ().GetLantern () != "NON" && player4.GetComponent<tutorialPlayerColour> ().GetLantern () != "NON") {
			tutorialStep2 ();
		}

	}

	void tutorialStep1 (){

		ImageBackground.SetActive (false);
		TextStart.SetActive (false);

		ImageBg.SetActive (true);
		ImageController.SetActive (true);
		TextCollect.SetActive (true);

		TextHeal.SetActive (false);

	}

	void tutorialStep2 (){

		ImageBackground.SetActive (false);
		TextStart.SetActive (false);

		ImageBg.SetActive (true);
		ImageController.SetActive (true);
		TextCollect.SetActive (false);

		TextHeal.SetActive (false);
		BottomBarrier.SetActive (false);

	}
}
