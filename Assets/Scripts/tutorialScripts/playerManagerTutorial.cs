﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class playerManagerTutorial : MonoBehaviour {

	private GameObject player1;
	private GameObject player2;
	private GameObject player3;
	private GameObject player4;

	private GameObject[] playersInLevel;

	public float maxPlayerHealth;
	public int healingPower;

	public float warningAtHealthPercentage;

	public string deadPlayer;

	public Transform canvas;

	//delegates are used to send information
	public delegate void PlayerHealth (bool dieing);
	public static event PlayerHealth dieing;


	void Start(){

		playersInLevel = GameObject.Find ("Level").GetComponent<levelManager> ().GetPlayersInLevel ();

		player1 = GameObject.Find ("player1");
		player2 = GameObject.Find ("player2");
		player3 = GameObject.Find ("player3");
		player4 = GameObject.Find ("player4");

		AssignInitialColours ();

		gameStats.control.playerWhoDied = "";

	}

	void Update(){
//		CheckForDieingPlayers ();
	}

	void OnEnable()
	{
		playerTutorial.died += EndOfGame;
		playerTutorial.colourNeeded += ReassignPlayerColour;
	}

	void OnDisable()
	{
		playerTutorial.died -= EndOfGame;
		playerTutorial.colourNeeded -= ReassignPlayerColour;
	}

	void EndOfGame(bool dead){
		if (dead == true) {

			CheckWhoDied ();

			//Debug.Log ("The game has ended");
			SceneManager.LoadScene (2);
		}else{
			//Continue as normal
		}
	}

	void AssignInitialColours(){

//		Debug.Log ("this has happened");

		player1.GetComponent<playerTutorial> ().SetColour ("RED");
		player2.GetComponent<playerTutorial> ().SetColour ("YEL");
		player3.GetComponent<playerTutorial> ().SetColour ("GRE");
		player4.GetComponent<playerTutorial> ().SetColour ("BLU");


	}
	void ReassignPlayerColour(){

		Debug.Log("reassign has happened?");
	

		player1.GetComponent<playerTutorial> ().SetColour ("RED");
		player2.GetComponent<playerTutorial> ().SetColour ("YEL");
		player3.GetComponent<playerTutorial> ().SetColour ("GRE");
		player4.GetComponent<playerTutorial> ().SetColour ("BLU");

//		List<string> currentColours = new List<string> ();
//		List<string> newColourList = new List<string> ();
//
//		string[] colourNames = GameObject.Find ("Level").GetComponent<colour> ().GetColourNames ();
//
//		//Get colour of players in the level
//		for (int l = 0; l < playersInLevel.Length; l++) {
//			if (playersInLevel [l].GetComponent<playerTutorial> ().GetColor () != "NON") {
//				currentColours.Add (playersInLevel [l].GetComponent<playerTutorial> ().GetColor ());
//			}
//		}
//
//		//Find a new random Colour
//		for (int j = 0; j < colourNames.Length; j++) {
//			if (!currentColours.Contains (colourNames [j]) && colourNames [j] != "NON") {
//				newColourList.Add (colourNames [j]);
//			}
//		}
//
//		//For a player who does not have a colour, assign a new colour
//		for (int p = 0; p < playersInLevel.Length; p++) {
//
//			int indexColour = (int)Random.Range (0f, newColourList.Count);
//
//			if (playersInLevel [p].GetComponent<playerTutorial> ().GetColor () == "NON") {
//				playersInLevel [p].GetComponent<playerTutorial> ().SetColour (newColourList[indexColour]);
//			} 
//		}
	}

	//This function looks for players who are running low on health and then broadcasts messages acordingly
//	void CheckForDieingPlayers ()
//	{
//
//		List<float> dieingPlayers = new List<float> ();
//
//		//Add any Hamsas's with 0 health into a list called deadHansas
//		for (int i = 0; i < playersInLevel.Length; i++) {
//
//			if ((playersInLevel [i].GetComponent<playerTutorial> ().playerHealth / maxPlayerHealth) < warningAtHealthPercentage) {
//
//				dieingPlayers.Add (playersInLevel [i].GetComponent<playerTutorial> ().playerHealth);
//
//				//dieing (true);
//
//			} else if ((playersInLevel [i].GetComponent<playerTutorial> ().playerHealth / maxPlayerHealth) <= warningAtHealthPercentage) {
//
//				dieingPlayers.Remove (playersInLevel [i].GetComponent<playerTutorial> ().playerHealth);
//
//			}
//		}
//
//		if (dieingPlayers.Count == 0) {
//			dieing (false);
//			//			Debug.Log ("false");
//		} else {
//			dieing (true);
//			//			Debug.Log ("true");
//		}
//	}

	void CheckWhoDied (){

		//Add any Players's with 0 health into a list called deadPlayers
		for (int i = 0; i < playersInLevel.Length; i++) {

			if (playersInLevel [i].GetComponent<playerTutorial> ().playerHealth <= 0) {

				gameStats.control.playerWhoDied += (playersInLevel [i].GetComponent<playerTutorial> ().playerName);

			}
		}

	}
}
