﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class tutorialHeal : MonoBehaviour {

	public string healInstruction;
	public string mixInstruction;


	//delegates are used to send information
	public delegate void RemovingColour ();
	public static event RemovingColour removeColour;

	//delegates are used to send information
	public delegate void ColourHasBeenReleased ();
	public static event ColourHasBeenReleased colourReleased;

	void OnTriggerStay(Collider coll) {
		switch (coll.gameObject.tag) {
		case "Player":
			if (Input.GetButtonDown (healInstruction)) {
				HealPlayer (coll.gameObject);
			}
			break;
		case "Hamsa":
			if (Input.GetButtonDown (healInstruction)) {
				HealHamsa (coll.gameObject);
			}
			break;
		}
	}

	void HealPlayer(GameObject player) {
		if (player.GetComponent<tutorialPlayerColour> ().GetColor () == GetComponent<tutorialPlayerColour> ().GetLantern ()) {

			player.GetComponent<tutorialPlayerColour> ().Healing ();
			//Debug.Log ("player matches lantern");

			//Remove colour from specific lantern
			GetComponent<tutorialPlayerColour>().lanternColour = "NON";
			removeColour();

			//check to see if a floor tile needs re-colouring
			//colourReleased ();

		}
		//else if player colour matchs part of the cure


	}

	void HealHamsa(GameObject hamsa) {
		if (hamsa.GetComponent<hamsa> ().GetColor () == GetComponent<tutorialPlayerColour> ().GetLantern ()) {

			hamsa.GetComponent<hamsa> ().Healing ();
			//Debug.Log ("player matches lantern");

			//Remove colour from specific lantern
			GetComponent<tutorialPlayerColour>().lanternColour = "NON";
			removeColour();

			//check to see if a floor tile needs re-colouring
			colourReleased ();
		}
		//else if player colour matchs part of the cure
	}
}
