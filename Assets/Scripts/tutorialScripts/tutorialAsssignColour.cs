﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class tutorialAsssignColour : MonoBehaviour {

	private GameObject[] playersInLevel;
	private string newPlayerColour;

	void OnEnable()
	{
		tutorialPlayerColour.colourNeeded += ReassignPlayerColour;
	}

	void OnDisable()
	{
		tutorialPlayerColour.colourNeeded -= ReassignPlayerColour;
	}

	void Start(){
		playersInLevel = GameObject.FindGameObjectsWithTag ("Player");
	}

	void ReassignPlayerColour(string playerName){

		switch (playerName) {
		case "player1":
			newPlayerColour = "GRE";	
			break;
		case "player2":
			newPlayerColour = "PUR";	
			break;
		case "player3":
			newPlayerColour = "YEL";	
			break;
		case "player4":
			newPlayerColour = "RED";	
			break;
		}

		for (int p = 0; p < playersInLevel.Length; p++) {

			if (playersInLevel [p].GetComponent<tutorialPlayerColour> ().GetColor () == "NON") {
				playersInLevel [p].GetComponent<tutorialPlayerColour> ().SetColour (newPlayerColour);
			} 
		}
	}
}
