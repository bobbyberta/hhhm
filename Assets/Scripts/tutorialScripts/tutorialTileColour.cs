﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class tutorialTileColour : MonoBehaviour {

	public string tileColour;

	public void SetColour(string colour) {
		tileColour = colour;
	}

	public string GetColor() {
		return tileColour;
	}

	void Awake() {
//		tileColour = tileColour;
	}

	void UpdateColour() {

		GetComponent<Renderer> ().material.color = GameObject.Find ("Level").GetComponent<colour> ().GetColours () [tileColour];

	}

	void Update(){
		UpdateColour ();

	}
}
