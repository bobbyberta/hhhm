﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class tutorialScriptTopRight: MonoBehaviour {

	public GameObject TopRight;
	public GameObject ImageBackground;
	public GameObject TextStart;
	public GameObject ImageBg;
	public GameObject ImageController;
	public GameObject TextCollect;
	public GameObject TextHeal;
	public GameObject TopBarrier;

	private GameObject player1;
	private GameObject player2;



	// Use this for initialization
	void Start () {

		player1 = GameObject.Find ("player1");
		player2 = GameObject.Find ("player2");

		tutorialStep1 ();
	
		
	}
		

	// Update is called once per frame
	void Update () {

		if (player1.GetComponent<tutorialPlayerColour> ().GetLantern () != "NON" && player2.GetComponent<tutorialPlayerColour> ().GetLantern () != "NON") {
			tutorialStep2 ();
		}
		
	}

	void tutorialStep1 (){

		ImageBackground.SetActive (false);
		TextStart.SetActive (false);

		ImageBg.SetActive (true);
		ImageController.SetActive (true);
		TextCollect.SetActive (true);

		TextHeal.SetActive (false);

	}

	void tutorialStep2 (){

		ImageBackground.SetActive (false);
		TextStart.SetActive (false);

		ImageBg.SetActive (false);
		ImageController.SetActive (false);
		TextCollect.SetActive (false);

		TextHeal.SetActive (true);
		TopBarrier.SetActive (false);

	}
}
