﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class colour : MonoBehaviour {

	private Dictionary<string, Color> colours;
	private Dictionary<string, Color> primaryColours;
	private Dictionary<string, List<string>> colourMix;
	private string[] colourNames;
	private string[] primaryColourNames;

	void Awake(){

		Color NON = new Vector4 (1f, 1f, 1f, 1f);
		Color RED = new Vector4 (0.92f, 0.216f, 0.26f, 1f);
		Color ORA = new Vector4 (0.968f, 0.464f, 0.012f, 1f);
		Color YEL = new Vector4 (0.996f, 0.808f, 0.06f, 1f);
		Color GRE = new Vector4 (0.02f, 0.46f, 0.342f, 1f);
		Color BLU = new Vector4 (0.18f, 0.708f, 0.808f, 1f);
		Color PUR = new Vector4 (0.468f, 0.108f, 0.336f, 1f);

		colours = new Dictionary<string, Color> (){
			{"NON", NON},
			{"RED", RED},
			{"ORA", ORA},
			{"YEL", YEL},
			{"GRE", GRE},
			{"BLU", BLU},
			{"PUR", PUR}
		};
//
//		primaryColours = new Dictionary<string, Color> () {{ "NON", NON },
//			{ "RED", RED },
//			{ "YEL", YEL },
//			{ "BLU", BLU }};
//		
//		colourMix = new Dictionary<string, List<string>> () {{"NON", new List<string>{"NON"}},
//			{"RED", new List<string>{"RED"}}, 
//			{"ORA", new List<string>{"ORA", "REDYEL"}},
//			{"YEL", new List<string>{"YEL"}},m
//			{"GRE", new List<string>{"GRE", "YELBLU"}},
//			{"BLU", new List<string>{"BLU"}},
//			{"PUR", new List<string>{"PUR", "REDBLU"}}};
		
		colourNames     = new string[]{"NON", "RED", "ORA", "YEL", "GRE", "BLU", "PUR"};
		primaryColourNames = new string[]{"NON", "RED", "YEL", "BLU"};

//		Debug.Log (colourNames);
	}

	public Dictionary<string, Color> GetColours() {
		return colours;
	}

	public string[] GetColourNames() {
		return colourNames;
	}

//	public Dictionary<string, Color> GetPrimColours() {
//		return primaryColours;
//	}
//
//	public Dictionary<string, List<string>> GetColourCures() {
//		return colourMix;
//	}
//
//
//	public string[] GetPrimColourNames() {
//		return primaryColourNames;
//	}


}
