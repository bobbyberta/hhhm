﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class unPauseGame : MonoBehaviour {

	public void UnPauseGame(){

		gameStats.control.paused = false;
		gameStats.control.tutorialMode = false;

		GameObject [] pauseUI = GameObject.Find ("Players").GetComponent<quit> ().pauseUI;
		GameObject [] players = GameObject.Find ("Players").GetComponent<quit> ().players;

		foreach (GameObject player in players)
		{
			player.SetActive (true);
		}

			foreach (GameObject show in pauseUI)
			{
				show.SetActive (false);
			}

		Time.timeScale = 1;
	
	}
}
