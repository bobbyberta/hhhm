﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class attack : MonoBehaviour
{
	public int attackDelay;
	public int hamsaStartAttackValue;

	private int hamsaCurrentAttackValue;
	private int hamsaDeadAttackValue;
	private float countDown;

	//delegates are used to send information
	public delegate void AttackDamage (int hamsaAttackValue);
	public static event AttackDamage timeUp;


	void Start ()
	{
		countDown = attackDelay;
		hamsaCurrentAttackValue = hamsaStartAttackValue;
		hamsaDeadAttackValue = 0;
	}

	void OnEnable ()
	{
		hamsaManager.broken += StopAttacking;
	}


	void OnDisable ()
	{
		hamsaManager.broken -= StopAttacking;
	}

	void StopAttacking (bool broken)
	{
		if (broken == true) {
			hamsaCurrentAttackValue = hamsaDeadAttackValue;
			//Debug.Log("broken is true");
		} else if (broken == false) {
			hamsaCurrentAttackValue = hamsaStartAttackValue;
			//Debug.Log("broken is false");
		}else {
			//Debug.Log ("Something has gone wrong");
		}
	}

	void Update ()
	{

		countDown -= Time.deltaTime;

		if (Mathf.Floor (countDown) <= 0) {

			if (timeUp != null)
				timeUp (hamsaCurrentAttackValue);

			//Reset the countDown timer
			countDown = attackDelay;

		}
	}
}
