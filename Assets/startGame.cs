﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class startGame : MonoBehaviour {

	public void StartGame(){

		gameStats.control.paused = false;
		gameStats.control.tutorialMode = false;

		GameObject [] players = GameObject.Find ("Level").GetComponent<levelManager> ().players;
		GameObject [] sounds = GameObject.Find ("Level").GetComponent<levelManager> ().sounds;
		GameObject [] tutorialUI = GameObject.Find ("Level").GetComponent<levelManager> ().tutorialUI;

		foreach (GameObject player in players)
		{
			player.SetActive (true);
		}

		foreach (GameObject sound in sounds)
		{
			sound.SetActive (true);
		}

		foreach (GameObject show in tutorialUI)
		{
			show.SetActive (false);
		}

		Time.timeScale = 1;

	}
}
